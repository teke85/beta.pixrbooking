import prisma from '@/app/libs/prismadb';

export const getListingById = async (listingId: string) => {
  try {
    const listing = await prisma.listing.findUnique({
      where: {
        id: listingId,
      },
      include: {
        rooms: true,
        propertyType: true,
      },
    });

    if (!listing) return null;

    return listing;
  } catch (error: any) {
    throw new Error(error);
  }
};

export default getListingById;
