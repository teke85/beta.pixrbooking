'use client';

import Link from 'next/link';
import React, { useState, useEffect } from 'react';
import Logo from './Logo';
import SearchMenu from '../SearchMenu';
import { BiSearch } from 'react-icons/bi';
import UserMenu from './UserMenu';
import Image from 'next/image';
import { User } from '@prisma/client';
import Categories from './Categories';

interface NavProps {
  currentUser?: User | null;
}

const Nav: React.FC<NavProps> = ({ currentUser }) => {
  const [header, setHeader] = useState(false);
  const [showSearch, setShowSearch] = useState(false);
  const [showDropDown, setShowDropDown] = useState(false);

  const scrollHeader = () => {
    if (window.scrollY >= 20) {
      setHeader(true);
    } else {
      setHeader(false);
    }
  };

  const toggleSearch = () => {
    setShowSearch(!showSearch);
  };

  const toggleDropDown = () => {
    setShowDropDown(!showDropDown);
  };

  useEffect(() => {
    window.addEventListener('scroll', scrollHeader);

    return () => {
      window.removeEventListener('scroll', scrollHeader);
    };
  }, []);

  return (
    <div
      className={`p-2 shadow-md z-50
        ${
          header
            ? 'fixed w-full bg-[#2e3236] text-white transition-all duration-300'
            : 'bg-[transparent]'
        }
      `}
    >
      <div className="header flex w-[100%] items-center justify-between m-auto">
        <div className="flex items-center logo">
          <Link href="/">
            <Logo />
          </Link>
          <span className="text2 text-black font-bold font-play-fair text-[20px]">
            PixrBooking
          </span>
        </div>
        <div
          className="search-icon rounded-full p-2 bg-slate-500 hover:scale-105 transition-transform duration-300 transform cursor-pointer"
          onClick={toggleSearch}
        >
          <BiSearch size={18} />
        </div>
        <div className="menu hidden md:flex">
          <nav className="flex items-center text-[14px]">
            <ul className="flex items-center gap-3">
              <li>
                <Link
                  className={`font-semibold hover:text-gray-700 transition-transform duration-300 transform ${
                    header
                      ? 'hover:text-white transition-all'
                      : 'hover:text-gray-700'
                  }`}
                  href="/hotels"
                >
                  Hotels & Lodges
                </Link>
              </li>
              <li>
                <Link
                  className={`font-semibold hover:text-gray-700 transition-transform duration-300 transform ${
                    header
                      ? 'hover:text-white transition-all'
                      : 'hover:text-gray-700'
                  }`}
                  href="/travel"
                >
                  Travel
                </Link>
              </li>
              <li>
                <Link
                  className={`font-semibold hover:text-gray-700 transition-transform duration-300 transform ${
                    header
                      ? 'hover:text-white transition-all'
                      : 'hover:text-gray-700'
                  }`}
                  href="/directory"
                >
                  Directory
                </Link>
              </li>
              <li>
                <Link
                  className={`font-semibold hover:text-gray-700 transition-transform duration-300 transform ${
                    header
                      ? 'hover:text-white transition-all'
                      : 'hover:text-gray-700'
                  }`}
                  href="/car-rental"
                >
                  Car Rentals
                </Link>
              </li>
              <li className="relative font-semibold">
                {/* Property type link */}
                <span
                  className={`flex items-center cursor-pointer font-semibold hover:text-gray-700 ${
                    header ? 'hover:text-white' : 'hover:text-gray-700'
                  }`}
                  onClick={toggleDropDown}
                >
                  Property type
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 text-gray-500 inline-block ml-1 transition-transform duration-300 transform rotate-180 hover:rotate-[180]"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M6.293 9.293a1 1 0 011.414 0L10 11.586l2.293-2.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </span>
                {/* Dropdown menu */}
                <div
                  className={`text-gray-400 absolute cursor-pointer ${
                    showDropDown ? 'block' : 'hidden'
                  } space-y-2 w-[500px] bg-white border overflow-y-scroll px-2 border-gray-300 rounded-lg shadow-lg`}
                >
                  <ul className="relative h-[50vh] w-50">
                    <li className="flex gap-4 justify-between py-10">
                      <Link
                        className="block px-4 py-2 hover:bg-gray-500 hover:text-white"
                        href="/luxury-apartment"
                      >
                        Luxury Apartments
                      </Link>
                      <span className="relative w-fit">
                        <Image
                          src="/images/resort.jpg"
                          width={200}
                          height={200}
                          objectFit="contain"
                          alt="resort"
                        />
                      </span>
                    </li>

                    <li className="flex gap-4 justify-between py-10">
                      <Link
                        className="block px-4 py-2 hover:bg-gray-500 hover:text-white"
                        href="/lodges"
                      >
                        Lodges
                      </Link>
                      <span className="relative w-fit">
                        <Image
                          src="/images/villa.jpg"
                          width={200}
                          height={100}
                          objectFit="contain"
                          alt="resort"
                          className="rounded-full"
                        />
                      </span>
                    </li>
                    <li className="flex gap-4 justify-between py-10">
                      <Link
                        className="block px-4 py-2 hover:bg-gray-500 hover:text-white"
                        href="/villa"
                      >
                        Villas
                      </Link>
                      <span className="relative w-fit">
                        <Image
                          src="/images/villa.jpg"
                          width={100}
                          height={50}
                          objectFit="contain"
                          alt="resort"
                        />
                      </span>
                    </li>

                    <Link
                      className="block px-4 py-2 hover:bg-gray-500 hover:text-white"
                      href="/student-accomodation"
                    >
                      Student Accomodation
                    </Link>
                    <Link
                      className="block px-4 py-2 hover:bg-gray-500 hover:text-white"
                      href="/flats"
                    >
                      Flats
                    </Link>
                    <Link
                      className="block px-4 py-2 hover:bg-gray-500 hover:text-white"
                      href="/cottages"
                    >
                      Cottages
                    </Link>
                    <Link
                      className="block px-4 py-2  hover:bg-gray-500 hover:text-white"
                      href="/houses-for-rent"
                    >
                      Houses for Rent
                    </Link>
                    <Link
                      className="block px-4 py-2  hover:bg-gray-500 hover:text-white"
                      href="/campsites"
                    >
                      Camp Sites
                    </Link>
                  </ul>
                </div>
              </li>
            </ul>
          </nav>
        </div>
        <div
          className={
            header
              ? 'bg-transparent text-black transition-all duration-300 rounded-md'
              : 'bg-[transparent]'
          }
        >
          <UserMenu currentUser={currentUser} />
        </div>
      </div>
      <Categories />
      <div className="flex bg-white transition-all duration-300">
        {showSearch && <SearchMenu />}
      </div>
    </div>
  );
};

export default Nav;
