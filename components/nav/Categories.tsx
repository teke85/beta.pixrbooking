'use client';

import { TbBeach } from 'react-icons/tb';
import { FaHotel } from 'react-icons/fa';
import { GiWindmill, GiBed } from 'react-icons/gi';
import { AiFillHome } from 'react-icons/ai';
import { FaUserGraduate } from 'react-icons/fa';
import { BiHomeHeart, BiDoorOpen } from 'react-icons/bi';
import { FaCampground } from 'react-icons/fa';
import { MdOutlineVilla } from 'react-icons/md';
import Container from '../Container';
import CategoryBox from '../CategoryBox';
import { usePathname, useSearchParams } from 'next/navigation';

export const categories = [
  {
    label: 'Hotel',
    icon: FaHotel,
    description: 'This property is a Premium Hotel!',
  },
  {
    label: 'Lodge',
    icon: BiHomeHeart,
    description: 'This property is a Lodge!',
  },
  {
    label: 'House',
    icon: AiFillHome,
    description: 'This property is a House!',
  },
  {
    label: 'Villa',
    icon: MdOutlineVilla,
    description: 'This property is a Villa!',
  },
  {
    label: 'Cottage',
    icon: BiDoorOpen,
    description: 'This property is a Cottage!',
  },
  {
    label: 'Student Accomodation',
    icon: FaUserGraduate,
    description: 'This property is a Lodge!',
  },
  {
    label: 'Flat',
    icon: AiFillHome,
    description: 'This property is a Lodge!',
  },
  {
    label: 'Bed & Breakfast',
    icon: GiBed,
    description: 'This property is a Lodge!',
  },
  {
    label: 'Camp Site',
    icon: FaCampground,
    description: 'This property is a Lodge!',
  },
  {
    label: 'Backpackers Hostel',
    icon: FaCampground,
    description: 'This property is a Backpackers Hostel!',
  },
];

const Categories = () => {
  const params = useSearchParams();
  const category = params?.get('category');
  const pathname = usePathname();

  const isMainPage = pathname === '/';

  if (!isMainPage) {
    return null;
  }

  return (
    <Container>
      <div
        className="
      pt-2
      flex 
      flex-row 
      items-center 
      justify-between 
      overflow-auto"
      >
        {categories.map((item) => (
          <CategoryBox
            key={item.label}
            label={item.label}
            selected={category === item.label}
            icon={item.icon}
          />
        ))}
      </div>
    </Container>
  );
};

export default Categories;
