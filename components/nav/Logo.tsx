'use client';

import Image from 'next/image';
import { useRouter } from 'next/navigation';

const Logo = () => {
  const router = useRouter();
  return (
    <Image
      src="/images/logo.png"
      className=""
      alt="Logo"
      height="60"
      width="60"
    />
  );
};

export default Logo;
