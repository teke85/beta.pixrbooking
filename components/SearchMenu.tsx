'use client';

import React, { useState, useEffect } from 'react';

const SearchMenu = () => {
  const [header, setHeader] = useState(false);

  const scrollHeader = () => {
    if (window.scrollY >= 20) {
      setHeader(true);
    } else {
      setHeader(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', scrollHeader);

    return () => {
      window.removeEventListener('scroll', scrollHeader);
    };
  }, []);

  return (
    <div className="search-component p-2 w-full flex flex-col mx-auto items-center justify-center">
      <div className="flex flex-col md:flex-row gap-2 justify-evenly search-form hover:shadow-md border-[1px] w-full p-4 transition">
        <input
          className={`border p-2 rounded-full text-sm font-semibold cursor-pointer focus:outline-none ${
            header ? 'text-white' : 'text-black'
          }
              `}
          type="text"
          placeholder="Where are you going??..."
        />
        <input
          className={`border p-2 rounded-full w-full md:w-1/4 cursor-pointer focus:outline-none ${
            header ? 'text-black' : 'text-black'
          }`}
          type="date"
          placeholder="Check-in"
        />
        <input
          className={`border p-2 rounded-full w-full md:w-1/4 cursor-pointer focus:outline-none ${
            header ? 'text-black' : 'text-black'
          }`}
          type="date"
          placeholder="Check-out"
        />
        <select
          className={`bg-transparent focus:outline-none ${
            header ? 'text-black' : 'text-black'
          }`}
        >
          <option className={header ? 'text-black' : 'text-black'} value="">
            Guests
          </option>
          <option className={header ? 'text-black' : 'text-black'} value="1">
            1 Guest
          </option>
          <option className={header ? 'text-black' : 'text-white'} value="2">
            2 Guests
          </option>
          <option className={header ? 'text-black' : 'text-white'} value="3">
            3 Guests
          </option>
          <option className={header ? 'text-black' : 'text-white'} value="4">
            4 Guests
          </option>
          {/* Add more options as needed */}
        </select>
        <button className="border p-2 rounded-full w-full md:w-[15%] text-white bg-[#364250]">
          Search
        </button>
      </div>
      <div className="additional-options">
        {/* Additional options such as filters, sorting, etc. */}
      </div>
    </div>
  );
};

export default SearchMenu;
