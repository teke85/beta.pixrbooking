'use client';

import Select from 'react-select';
import useCountries from '@/components/hooks/useCountries';

export type CountrySelectValue = {
  flag: string;
  label: string;
  latlng: number[];
  region: string;
  value: string;
};

interface CountrySelectProps {
  value?: CountrySelectValue;
  onChange: (value: CountrySelectValue) => void;
}

const CountrySelect: React.FC<CountrySelectProps> = ({ value, onChange }) => {
  const { getAll } = useCountries();

  const handleChange = (newValue: CountrySelectValue | null) => {
    if (newValue !== null) {
      onChange(newValue);
    } else {
      // Handle the case when the select is cleared
      onChange({ flag: '', label: '', latlng: [], region: '', value: '' });
    }
  };

  return (
    <div>
      <Select
        placeholder="Search"
        isClearable
        options={getAll()}
        value={value}
        onChange={handleChange}
        formatOptionLabel={(option: any) => (
          <div className="flex flex-row items-center gap-3">
            <div>
              {option.flag}
            </div>
            <div>
              {option.label},
              <span className="text-neutral-500 ml-1">{option.region}</span>
            </div>
          </div>
        )}
      />
    </div>
  );
};

CountrySelect.defaultProps = {
  onChange: (value: CountrySelectValue) => {
    console.log('Default onChange function triggered.');
  },
};

export default CountrySelect;
