'use client'

import Image from 'next/image';

const Avatar = () => {
return (
    <Image
      src="/images/placeholder.jpg"
      className="rounded-full"
      alt="Logo"
      height="20"
      width="20"
    />
  );
};

export default Avatar;
