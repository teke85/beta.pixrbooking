import RegisterModal from '@/components/modals/RegisterModal';
import ToasterProvider from '@/app/providers/ToasterProvider';
import Nav from '@/components/nav/Nav';
import type { Metadata } from 'next';
import { Montserrat, Italiana, Playfair_Display } from 'next/font/google';
import './globals.css';
import LoginModal from '@/components/modals/LoginModal';
import getCurrentUser from '@/actions/getCurrentUser';
import RentModal from '@/components/modals/RentModal';

const montserrat = Montserrat({
  subsets: ['latin'],
  variable: '--font-montserrat',
});

export const italiana = Italiana({
  subsets: ['latin'],
  display: 'swap',
  weight: '400',
  variable: '--font-italiana',
});

const play_fair = Playfair_Display({
  subsets: ['latin'],
  variable: '--font-play_fair',
  weight: '500',
  display: 'swap',
});

export const metadata: Metadata = {
  title: 'PixrBooking',
  description: 'Online booking platform',
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const currentUser = await getCurrentUser();
  return (
    <html lang="en">
      <body
        className={`${montserrat.variable} ${play_fair.variable} ${italiana.variable}`}
      >
        <ToasterProvider />
        <RentModal />
        <RegisterModal />
        <LoginModal />
        <Nav currentUser={currentUser} />
        {children}
      </body>
    </html>
  );
}
