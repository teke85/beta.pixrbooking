import Nav from '@/components/nav/Nav';
import Image from 'next/image';
import { Button } from '@/components/ui/button';
import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from '@/components/ui/carousel';
import {
  Card,
  MyCard,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/ui/card';

export default function Home() {
  return (
    <div>
      <div className="flex flex-col h-[100vh] gap-5">
        <h2>Welcome</h2>
        <div className="flex justify-center w-full">
          <Button className="w-fit">Click me</Button>
        </div>
        <Button className="sm">Click me</Button>
        <div className="flex justify-center bg-teal-500 w-full">
          <Carousel
            opts={{
              align: 'start',
            }}
            className="w-full max-w-sm"
          >
            <CarouselContent>
              {Array.from({ length: 10 }).map((_, index) => (
                <CarouselItem key={index} className="md:basis-1/2 lg:basis-1/3">
                  <div className="p-1">
                    <Card>
                      <CardContent className="flex aspect-square items-center justify-center p-6">
                        <span className="text-3xl font-semibold">
                          {index + 1}
                        </span>
                      </CardContent>
                    </Card>
                  </div>
                </CarouselItem>
              ))}
            </CarouselContent>
            <CarouselPrevious />
            <CarouselNext />
          </Carousel>
        </div>

        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ducimus
          eligendi perspiciatis quasi adipisci suscipit, atque in minima ipsam!
          Ea dignissimos fuga aut adipisci! In voluptates dolorem ullam, nostrum
          unde numquam consequuntur doloremque aut. Ab sequi, deleniti nemo
          animi numquam nisi. Eveniet distinctio corrupti ea magnam voluptate
          suscipit molestias quos, illum non odit sit dolorum minus, soluta sed
          quibusdam perspiciatis minima exercitationem ad voluptatem quaerat
          quis possimus. A ducimus accusamus temporibus quia error neque qui ea!
          Eos, adipisci a voluptates asperiores sequi et repudiandae similique
          beatae doloribus quibusdam dolorem officia, ut magnam ipsa excepturi
          voluptatem odit at laboriosam praesentium quas neque. Lorem ipsum
          dolor sit amet consectetur, adipisicing elit. Ducimus eligendi
          perspiciatis quasi adipisci suscipit, atque in minima ipsam! Ea
          dignissimos fuga aut adipisci! In voluptates dolorem ullam, nostrum
          unde numquam consequuntur doloremque aut. Ab sequi, deleniti nemo
          animi numquam nisi. Eveniet distinctio corrupti ea magnam voluptate
          suscipit molestias quos, illum non odit sit dolorum minus, soluta sed
          quibusdam perspiciatis minima exercitationem ad voluptatem quaerat
          quis possimus. A ducimus accusamus temporibus quia error neque qui ea!
          Eos, adipisci a voluptates asperiores sequi et repudiandae similique
          beatae doloribus quibusdam dolorem officia, ut magnam ipsa excepturi
          voluptatem odit at laboriosam praesentium quas neque. Lorem ipsum
          dolor sit amet consectetur, adipisicing elit. Ducimus eligendi
          perspiciatis quasi adipisci suscipit, atque in minima ipsam! Ea
          dignissimos fuga aut adipisci! In voluptates dolorem ullam, nostrum
          unde numquam consequuntur doloremque aut. Ab sequi, deleniti nemo
          animi numquam nisi. Eveniet distinctio corrupti ea magnam voluptate
          suscipit molestias quos, illum non odit sit dolorum minus, soluta sed
          quibusdam perspiciatis minima exercitationem ad voluptatem quaerat
          quis possimus. A ducimus accusamus temporibus quia error neque qui ea!
          Eos, adipisci a voluptates asperiores sequi et repudiandae similique
          beatae doloribus quibusdam dolorem officia, ut magnam ipsa excepturi
          voluptatem odit at laboriosam praesentium quas neque. Lorem ipsum
          dolor sit amet consectetur, adipisicing elit. Ducimus eligendi
          perspiciatis quasi adipisci suscipit, atque in minima ipsam! Ea
          dignissimos fuga aut adipisci! In voluptates dolorem ullam, nostrum
          unde numquam consequuntur doloremque aut. Ab sequi, deleniti nemo
          animi numquam nisi. Eveniet distinctio corrupti ea magnam voluptate
          suscipit molestias quos, illum non odit sit dolorum minus, soluta sed
          quibusdam perspiciatis minima exercitationem ad voluptatem quaerat
          quis possimus. A ducimus accusamus temporibus quia error neque qui ea!
          Eos, adipisci a voluptates asperiores sequi et repudiandae similique
          beatae doloribus quibusdam dolorem officia, ut magnam ipsa excepturi
          voluptatem odit at laboriosam praesentium quas neque.
        </p>
      </div>
      <div className="flex justify-center h-[100vh] items-center flex-col gap-5">
        <h2>Welcome</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ducimus
          eligendi perspiciatis quasi adipisci suscipit, atque in minima ipsam!
          Ea dignissimos fuga aut adipisci! In voluptates dolorem ullam, nostrum
          unde numquam consequuntur doloremque aut. Ab sequi, deleniti nemo
          animi numquam nisi. Eveniet distinctio corrupti ea magnam voluptate
          suscipit molestias quos, illum non odit sit dolorum minus, soluta sed
          quibusdam perspiciatis minima exercitationem ad voluptatem quaerat
          quis possimus. A ducimus accusamus temporibus quia error neque qui ea!
          Eos, adipisci a voluptates asperiores sequi et repudiandae similique
          beatae doloribus quibusdam dolorem officia, ut magnam ipsa excepturi
          voluptatem odit at laboriosam praesentium quas neque.
        </p>
      </div>
    </div>
  );
}
